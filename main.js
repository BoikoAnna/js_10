//Реалізувати перемикання вкладок (таби) на чистому Javascript.

//Технічні вимоги:
//У папці tabs лежить розмітка для вкладок. 
//Потрібно, щоб після натискання на вкладку відображався конкретний текст для потрібної вкладки.
// При цьому решта тексту повинна бути прихована. 
//У коментарях зазначено, який текст має відображатися для якої вкладки.
//Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.
//Потрібно передбачити, що текст на вкладках може змінюватись, і що вкладки можуть додаватися
// та видалятися. При цьому потрібно, щоб функція, написана в джаваскрипті, 
//через такі правки не переставала працювати.


let tab=function(){
let tabNav = document.querySelectorAll('.tabs-nav_item'),
tabContent=document.querySelectorAll('.tab'),
tabName;

tabNav.forEach (item => {
item.addEventListener('click', selectTabNav)
});

function selectTabNav(){
tabNav.forEach (item => {
item.classList.remove('is-active')
});
this.classList.add('is-active');
tabName=this.getAttribute('data-tab-name');
selectTabContent(tabName);
};

function selectTabContent (tabName) {
tabContent.forEach(item =>{
item.classList.contains(tabName) ? item.classList.add('is-active') : item.classList.remove('is-active');
console.log(tabName);
})
}

};

tab();
